<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Company extends Model
{
    protected  $fillable = ['title', 'email', 'logo', 'website'];

    public static function uploadImage(Request $request, $image = null)
    {
        if ($request->hasFile('logo')) {
            if ($image) {
                Storage::delete($image);
            }
            return $request->file('logo')->store("/");
        }
        return null;
    }

    public function getImage()
    {
        if (!$this->logo) {
            return asset("no-image.png");
        }
        return asset("storage/app/public/{$this->logo}");
    }
}
