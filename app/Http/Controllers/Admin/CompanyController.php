<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $companies = Company::paginate(10);
        return view('admin.companies.index', compact('companies'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'email' => 'required',
            'logo' => 'nullable|image|dimensions:min_width=100,min_height=100',
        ]);

        $data = $request->all();

        $data['logo'] = Company::uploadImage($request);

        $company = Company::create($data);
        return  redirect()->route('companies.index')->with('success', 'Сompany added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('admin.companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'email' => 'required',
            'logo' => 'nullable|image|dimensions:min_width=100,min_height=100',
        ]);

        $company = Company::find($id);
        $data = $request->all();

        $data['company'] = Company::uploadImage($request, $company->logo);

        if ($file = Company::uploadImage($request, $company->logo)) {
            $data['logo'] = $file;
        }

        $company->update($data);
        return  redirect()->route('companies.index')->with('success', 'Changes saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        Storage::delete($company->logo);
        $company->delete();
        return  redirect()->route('companies.index')->with('success', 'Сompany deleted');
    }
}
